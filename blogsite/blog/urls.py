from django.urls import path
from blog.views import home, about
from .views import (
    PostListView,
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,
)

# <app>/<model>_<viewtype>.html

urlpatterns = [
    path("", PostListView.as_view(), name="blog-home"),
    path("user/<str:username>", UserPostListView.as_view(), name="user-posts"),
    # will look for post_detail.html
    path("post/<int:pk>/", PostDetailView.as_view(), name="post-detail"),
    # will expect and look for post_form.html
    path("post/new/", PostCreateView.as_view(), name="post-create"),
    # will expect and look for post_form.html
    path("post/<int:pk>/update/", PostUpdateView.as_view(), name="post-update"),
    # will expect a form post_confirm_delete.html
    path("post/<int:pk>/delete/", PostDeleteView.as_view(), name="post-delete"),
    path("about/", about, name="blog-about"),
]
